using Xunit;
using Demo.DockerDeployment.Server.Controllers;
using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System.Linq;
using System;

namespace Demo.DockerDeployment.Server.Tests
{
    public class WeatherForecastControllerTests
    {
        Mock<IDbContextFactory<WeatherDbContext>> _mockFactory;
        ILogger<WeatherForecastController> _nullLogger;
        public WeatherForecastControllerTests()
        {
            _mockFactory = new Mock<IDbContextFactory<WeatherDbContext>>();
            _nullLogger = new NullLoggerFactory().CreateLogger<WeatherForecastController>();
        }

        [Fact]
        public void Get_DatabaseConnected_DataReturned()
        {
            _mockFactory.Setup(x => x.CreateDbContext())
                .Returns(new TestDbContext(new DbContextOptionsBuilder<WeatherDbContext>()
                    .UseInMemoryDatabase("TestDb").Options));
            InitializeDb(_mockFactory.Object);
            var contoller = new WeatherForecastController(_mockFactory.Object, _nullLogger);

            var actual = contoller.Get();

            Assert.True(actual.Count() == 5);
        }

        [Fact]
        public void Get_EmptyDatabase_EmptyArrayReturned()
        {
            _mockFactory.Setup(x => x.CreateDbContext())
                .Returns(new TestDbContext(new DbContextOptionsBuilder<WeatherDbContext>()
                    .UseInMemoryDatabase("TestDb").Options));
            var contoller = new WeatherForecastController(_mockFactory.Object, _nullLogger);

            var actual = contoller.Get();

            Assert.Empty(actual);
        }

        private void InitializeDb(IDbContextFactory<WeatherDbContext> dbContextFactory)
        {
            var context = dbContextFactory.CreateDbContext();
            context.AddRange(
                new Entities.WeatherForecast { Id = 1, Date = new DateTime(1234, 1, 1), TemperatureC = 0, Summary = "Bla" },
                new Entities.WeatherForecast { Id = 2, Date = new DateTime(1234, 1, 2), TemperatureC = 0, Summary = "Blu" },
                new Entities.WeatherForecast { Id = 3, Date = new DateTime(1234, 1, 3), TemperatureC = 0, Summary = "Bli" },
                new Entities.WeatherForecast { Id = 4, Date = new DateTime(1234, 1, 4), TemperatureC = 0, Summary = "Ble" },
                new Entities.WeatherForecast { Id = 5, Date = new DateTime(1234, 1, 5), TemperatureC = 0, Summary = "Blo" });
            context.SaveChanges();
        }
    }
}