﻿using System;
using Microsoft.EntityFrameworkCore;
using Demo.DockerDeployment.Server.Entities;
using System.Threading.Tasks;

namespace Demo.DockerDeployment.Server.Tests
{
    sealed internal class TestDbContext : WeatherDbContext
    {
        public TestDbContext(DbContextOptions<WeatherDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public override void Dispose()
        {
            Database.EnsureDeleted();
            base.Dispose();
        }
    }
}
