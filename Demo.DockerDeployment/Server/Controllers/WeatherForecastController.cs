using Demo.DockerDeployment.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Demo.DockerDeployment.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IDbContextFactory<WeatherDbContext> _contextFactory;

        public WeatherForecastController(IDbContextFactory<WeatherDbContext> contextFactory, ILogger<WeatherForecastController> logger)
        {
            _contextFactory = contextFactory;
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            IEnumerable<Entities.WeatherForecast> entities;
            using (var context = _contextFactory.CreateDbContext())
            {
                entities = context.WeatherForecasts?.Take(5).ToArray() ?? Array.Empty<Entities.WeatherForecast>();
            }

            return entities.Select(e => new WeatherForecast
            {
                Date = e.Date,
                TemperatureC = e.TemperatureC,
                Summary = e.Summary
            });
        }
    }
}