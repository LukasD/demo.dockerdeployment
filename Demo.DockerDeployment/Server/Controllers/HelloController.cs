﻿using Demo.DockerDeployment.Server.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Demo.DockerDeployment.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HelloController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private string _hello;

        public HelloController(IOptions<HelloOptions> hello, ILogger<WeatherForecastController> logger)
        {
            _hello = hello?.Value?.Hello ?? "<Hello>";
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            return _hello;
        }
    }
}
