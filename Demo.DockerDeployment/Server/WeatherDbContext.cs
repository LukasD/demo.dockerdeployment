﻿using Demo.DockerDeployment.Server.Entities;
using Microsoft.EntityFrameworkCore;

namespace Demo.DockerDeployment.Server
{
    public class WeatherDbContext : DbContext
    {
        public DbSet<WeatherForecast> WeatherForecasts { get; set; }

        public WeatherDbContext(DbContextOptions<WeatherDbContext> options) : base(options)
        {

        }
    }
}
