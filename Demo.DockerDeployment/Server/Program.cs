using Demo.DockerDeployment.Server;
using Demo.DockerDeployment.Server.Controllers;
using Demo.DockerDeployment.Server.Options;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the DI container.
builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

// Default configuration priority: appsettings.json < appsettings.{environment}.json < secrets.json < environment variables
// ":" is not supported in env, so "__" is used instead and it is automatically converted by dotnet.
// export DemoDockerDeployment_Database__ConnectionString="Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=demo;Integrated Security=True;"
builder.Configuration.AddEnvironmentVariables(prefix: "DemoDockerDeployment_");
// export DemoDockerDeployment_Hello__Hello="Tere"
builder.Services.Configure<HelloOptions>(opt => builder.Configuration.GetSection("Hello").Bind(opt));
builder.Services.AddDbContextFactory<WeatherDbContext>(options => 
    options.UseSqlServer(builder.Configuration["Database:ConnectionString"]));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();


app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");

app.Run();
