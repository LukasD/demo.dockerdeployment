﻿namespace Demo.DockerDeployment.Server.Entities
{
    public class WeatherForecast
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public string? Summary { get; set; }
    }
}
