# Demo.DockerDeployment #

1. Run app locally
    * Deploy database to LocalDB
    ```powershell
    $env:DemoDockerDeployment_Database__ConnectionString="Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=demo;Integrated Security=True;"
    cd .\Demo.DockerDeployment\Server\
    dotnet ef database update
    dotnet run
    ```
    * Run app
2. Add CI/CD pipeline
    * Configure build and test on CI/CD pipeline
    ```yml
    build-job:
        stage: build
        image: mcr.microsoft.com/dotnet/sdk:6.0
        script:
        - 'dotnet build ./Demo.DockerDeployment/Server'
    test-job:
        stage: test
        image: mcr.microsoft.com/dotnet/sdk:6.0
        script:
        - 'dotnet test ./Demo.DockerDeployment.Server.Tests'
    ```
    * Commit, push and observe execution in Gitlab
3. Generate Dockerfile
    * Generate Dockerfile using VS (Right-click on project Demo.DockerDeployment.Server > Add > Docker Support...)
    ![Visual Studio Docker Support](docs/Docker_support.png)
    * Make changes to generated Dockerfile for CI/CD and Kubernetes (see comments in ``./Demo.DockerDeployment/Dockerfile``)
    ```dockerfile
    ...
    EXPOSE 8000
    ENV ASPNETCORE_URLS=http://+:8000
    ```
    * Build using CI/CD pipeline
    ```yml
    release-build_image-job:
    stage: release
    image: docker:19.03.12
    variables:
        IMAGE_TAG: $CI_REGISTRY_IMAGE/app:latest
    services:
        - docker:19.03.12-dind
    script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build -t $IMAGE_TAG ./Demo.DockerDeployment
        - docker push $IMAGE_TAG
    ```
    * Inspect the GitLab Docker Registry
4. Prepare Kubernetes
    * Install kubectl (Windows)
    ```powershell
    choco install kubernetes-cli
    ```
    * Go to Rancher dashboard
        * https://rancher.cloud.e-infra.cz/
    * Download cluster config yaml
    ![Rancher config download](docs/Rancher_config.png)
    * Copy the config to $HOME/.kube/config
    ```yml
    apiVersion: v1
    kind: Config
    clusters:
    - name: "kuba-cluster"
    cluster:
        server: "https://rancher.cloud.e-infra.cz/k8s/..."
    users:
    - name: "kuba-cluster"
    user:
        token: <YOUR-TOKEN>
    contexts:
    - name: "kuba-cluster"
    context:
        user: "kuba-cluster"
        cluster: "kuba-cluster"
        namespace: daubner-ns
    current-context: "kuba-cluster"
    ```
    * (OPTIONAL) Set default namespace (otherwise it must be specified in ``kubectl`` commands)
    ```yml
    ...
    context:
        user: "kuba-cluster"
        cluster: "kuba-cluster"
        namespace: <YOUR-NS>
    current-context: "kuba-cluster"
    ```
    * Generate GitLab access token with ``read_registry`` scope (User Settings > Access Tokens)
    ![GitLab AccessToken](docs/GitLab_accesstoken.png)
    * Add credentials for GitLab Docker Repository to Kubernetes
    ```powershell
    kubectl create secret docker-registry regcred --docker-server=registry.gitlab.ics.muni.cz:443 --docker-username=<EMAIL> --docker-password=<ACCESS_TOKEN> --docker-email=<EMAIL>
    ```
5. Deploy web app without database
    * Set host name in ``./k8s/ingress.yaml`` to a non-existing domain name
    * Execute ``kubectl apply -f .`` in ``./k8s``
    * Open the app in browser
6. Configure the app
    * Set environment variable support in ``Program.cs``
    ```csharp
    builder.Configuration.AddEnvironmentVariables(prefix: "DemoDockerDeployment_");
    ```
    * Configure variables in your deployment
    ```yml
    ...
    containers:
        ...
        env:                                                                    
        - name: DemoDockerDeployment_Hello__Hello
            value: Tere
    ...
    ```
    * (ALTERNATIVE, not covered in demo) ConfigMaps
    * Generate Base64 representation of your connection string (remember to replace ``<YOUR_NAMESPACE>``)
    ```powershell
    $conStr = "Data Source=pv179-mssql-svc.<YOUR_NAMESPACE>.svc.cluster.local;Database=demo;User Id=sa;Password=MyC0m9l&xP@ssw0rd;"
    $base64Constr = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($conStr))
    echo $base64Constr
    ```
    * Add the Base64 to ``./k8s/secret.yaml``
    ```yaml
    ...
    data:
        connectionString: <YOUR_BASE64_CONNECTION STRING>
    ```
    * Configure connection string as a secret in your deployment
    ```yml
    ...
    containers:
        ...
        env:                                                                    
        - name: DemoDockerDeployment_Database__ConnectionString
          valueFrom:
            secretKeyRef:
              name: pv179-deployment-demo-secret
              key: connectionString
    ...
7. Deploy database
    * Expose ``pv179-mssql-svc`` by setting ``type: LoadBalancer``
    * Execute ``kubectl apply -f .`` in ``./k8s/sqlserver``
    * Get External IP of the service
    ```powershell
    kubectl get services
    ```
    * Deploy migration on kubernetes-hosted database
    ```powershell
    $env:DemoDockerDeployment_Database__ConnectionString="Data Source=<EXTERNAL_IP>,<SERVICE_PORT>;Database=demo;User Id=sa;Password=MyC0m9l&xP@ssw0rd;"
    cd .\Demo.DockerDeployment\Server\
    dotnet ef database update
    ```
    * Revert the exposure ``pv179-mssql-svc`` by setting ``type: ClusterIP``
    * Execute ``kubectl apply -f .`` in ``./k8s/sqlserver``
    * NOTE: This is a dirty way of migrating. You can use K8s Jobs for migrations in cluster.
8. Enjoy